import re
import string


class TextCleaner:
    text = """
    Sehr geehrter Herr Nielsen,

    welche Version setzen Sie ein? Ein Neustart von bit-MaRisk kann das Problem lösen.

    Hier kann natürlich auch noch Text stehen!
    Sollten Sie weitere Fragen haben, stehen wir Ihnen gerne zur Verfügung.

    Ihr bit-MaRisk-Supportteam
    """

    def clean(self, allow_none=False, duplicate_whitespace=True, remove_linebreaks_with='\n', to_ascii=True, allow_mentions=False, allow_urls=False, allow_digits=True, allow_punctuations=True):
        if self.text == None:
            try:
                if allow_none:
                    return None
            except ValueError:
                print("No text was given as input!\n")

        self.text = str(self.text)
        self.allow_mentions(allow_mentions)
        self.allow_urls(allow_urls)
        self.remove_linebreaks(remove_linebreaks_with)
        self.duplicate_whitespace(duplicate_whitespace)
        self.to_ascii(to_ascii)
        self.allow_digits(allow_digits)
        self.allow_punctuations(allow_punctuations)

        return self.text

    def allow_punctuations(self, allow_punctuations):
        if not allow_punctuations:
            for character in string.punctuation:
                self.text = self.text.replace(character, "")

    def allow_digits(self, allow_digits):
        if not allow_digits:
            self.text = re.sub("\d+", "", self.text)

    def to_ascii(self, to_ascii):
        if to_ascii:
            self.text = self.text.encode('ascii', 'ignore').decode()

    def duplicate_whitespace(self, duplicate_whitespace):
        if not duplicate_whitespace:
            self.text = re.sub(' +', ' ', self.text).strip()

    def remove_linebreaks(self, remove_linebreaks_with):
        if remove_linebreaks_with:
            self.text = re.sub(
                '\n+', remove_linebreaks_with, self.text).strip()

    def allow_urls(self, allow_urls):
        if not allow_urls:
            self.text = re.sub("https?:\/\/.*", "", self.text)

    def allow_mentions(self, allow_mentions):
        if not allow_mentions:
            self.text = re.sub("@\S+", "", self.text)


if __name__ == '__main__':
    textCleaner = TextCleaner()
    print(textCleaner.clean(textCleaner.text, allow_digits=True, allow_punctuations=True,
                            duplicate_whitespace=False, remove_linebreaks_with='\n', to_ascii=True))
